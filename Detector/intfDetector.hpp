#ifndef FD_VISIONCORE_OBJECTDETECTION_INTFDETECTOR_H
#define FD_VISIONCORE_OBJECTDETECTION_INTFDETECTOR_H

#include <QtCore>
#include <opencv2/opencv.hpp>



namespace FD {
namespace VisionCore {
namespace ObjectDetection {

typedef float Score_t;
typedef std::vector<std::vector<cv::Point>> Contours_t;
struct stuDetectedObject : public cv::Rect 
{
    stuDetectedObject() :
        category(0),
        score(1),
        contourIndex(0)
    { }

    stuDetectedObject(cv::Rect _rect, int _category = 0, Score_t _score = 1, uint _contourIndex = 0)  :
        cv::Rect(_rect),
        category(_category),
        score(_score),
        contourIndex(_contourIndex)
    { }

    int     category;
    Score_t score;
    uint    contourIndex;
};


class intfDetector
{
public:

public:
    intfDetector() :
        isInitialized(false)
    { }
    virtual ~intfDetector() { };

public:
    virtual bool init() = 0;
    virtual QList<stuDetectedObject> detectObjects(const cv::Mat& _image,
                                                   const Contours_t& _regionsOfInterest = Contours_t()) = 0;
    uint getInstanceIndex() { return instanceIndex; }

protected:
    uint  instanceIndex;
    bool isInitialized;
};


}
}
}

#endif // FD_VISIONCORE_OBJECTDETECTION_INTFDETECTOR_H
