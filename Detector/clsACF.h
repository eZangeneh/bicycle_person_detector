#ifndef FD_VISIONCORE_OBJECTDETECTION_CLSACF_H
#define FD_VISIONCORE_OBJECTDETECTION_CLSACF_H

#include "intfDetector.hpp"

namespace FD {
namespace VisionCore {
namespace ObjectDetection {

typedef QString FilePath_t;
class DetectorsManager;

namespace Private {
namespace ACF {

class clsACFPrivate;

}
}

class clsACF : public intfDetector
{
public:
    struct stuACFConfigs
    {
        stuACFConfigs() :
            detectionWindowsMinDistance         (4),
            classifierThreshold                 (-1),
            classifierCalibration                 (0),
            boundingBoxOverlapThresholdToUnify  (0.65),
            numberOfScalesPerOctave              (8),
            numberOfUpSampleOctave              (0),
            minimumDetectionSize                (cv::Size(41,100)),
            modelFilePath                       ("models/ped.xml"),
            readFromModel                       (1)
        { }

        int                 detectionWindowsMinDistance;
        float               classifierThreshold;
        float               classifierCalibration;
        float         boundingBoxOverlapThresholdToUnify;
        int                 numberOfScalesPerOctave;
        int                 numberOfUpSampleOctave;
        cv::Size            minimumDetectionSize;
        FilePath_t          modelFilePath;
        bool                readFromModel;
        Contours_t  regionsOfInterest;
    };

public:
    clsACF();
    ~clsACF();
public:
    bool init();
    QList<stuDetectedObject> detectObjects(const cv::Mat& _image,
                                           const Contours_t& _regionsOfInterest = Contours_t());
    static std::vector<stuACFConfigs> acfDetectorsConfigs;
    void setModelFilePath(QString path);



private:
    QScopedPointer<Private::ACF::clsACFPrivate> pPrivate;
    static int numberOfInstances;
};

}
}
}

#endif // FD_VISIONCORE_OBJECTDETECTION_CLSACF_H
