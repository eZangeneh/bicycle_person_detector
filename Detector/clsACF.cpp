#include "clsACF.h"
#include "Private/ACF/clsACFPrivate.h"

namespace FD {
namespace VisionCore {
namespace ObjectDetection {


int clsACF::numberOfInstances = 0;
std::vector<clsACF::stuACFConfigs> clsACF::acfDetectorsConfigs;
clsACF::clsACF() :
    pPrivate(new Private::ACF::clsACFPrivate)
{
    this->instanceIndex = clsACF::numberOfInstances++;
    if(uint(this->instanceIndex) >= clsACF::acfDetectorsConfigs.size())
        clsACF::acfDetectorsConfigs.resize(this->instanceIndex + 1);
}

clsACF::~clsACF()
{

}

bool clsACF::init()
{
    this->pPrivate->setConfigs(&(clsACF::acfDetectorsConfigs[this->instanceIndex]));
    bool wasSuccessfull = this->pPrivate->init();
    this->isInitialized = true;
    return wasSuccessfull;
}

QList<stuDetectedObject> clsACF::detectObjects(const cv::Mat &_image, const Contours_t& _regionsOfInterest)
{
    if(this->isInitialized == false)
        throw std::exception();
    QList<stuDetectedObject> detectionList = this->pPrivate->detectObjects(_image, _regionsOfInterest);
    return detectionList;
}

void clsACF::setModelFilePath(QString path)
{
    this->acfDetectorsConfigs[this->getInstanceIndex()].modelFilePath = path;
}



}
}
}

