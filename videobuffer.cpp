/*
 * Copyright (c) 2015 OpenALPR Technology, Inc.
 * Open source Automated License Plate Recognition [http://www.openalpr.com]
 * 
 * This file is part of OpenALPR.
 * 
 * OpenALPR is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License 
 * version 3 as published by the Free Software Foundation 
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include "videobuffer.h"
//#include <windows.h>
#include <QThread>


using namespace std;
void imageCollectionThread(void* arg);
void getImages(cv::VideoCapture cap, VideoDispatcher* dispatcher);


VideoBuffer::VideoBuffer()
{
  dispatcher = NULL;
  
}

VideoBuffer::~VideoBuffer()
{
  if (dispatcher != NULL)
  {
    dispatcher->active = false;
  }
}

VideoDispatcher* VideoBuffer::createDispatcher(std::string mjpeg_url, int fps)
{
  return new VideoDispatcher(mjpeg_url, fps);
}

void VideoBuffer::connect(std::string mjpeg_url, int fps)
{
  
    dispatcher = createDispatcher(mjpeg_url, fps);
      
    tthread::thread* t = new tthread::thread(imageCollectionThread, (void*) dispatcher);
    
}

int VideoBuffer::getLatestFrame(cv::Mat* frame)
{
  if (dispatcher == NULL)
    return -1;
  
  return dispatcher->getLatestFrame(frame);
}


void VideoBuffer::disconnect()
{
  if (dispatcher != NULL)
  {
    dispatcher->active = false;
  }
  
  dispatcher = NULL;
}





void imageCollectionThread(void* arg)
{
  
  VideoDispatcher* dispatcher = (VideoDispatcher*) arg;

  while (dispatcher->active)
  {
    try
    {
      cv::VideoCapture cap=cv::VideoCapture();
      dispatcher->log_info("Video stream connecting...");

     
      cap.open(dispatcher->mjpeg_url);
      
      
      if (cap.isOpened())
      {
        dispatcher->log_info("Video stream connected");
        getImages(cap, dispatcher);
      }
      else
      {
	std::stringstream ss;
	ss << "Stream " << dispatcher->mjpeg_url << " failed to open.";
	dispatcher->log_error(ss.str());
      }
	
    }
    catch (const std::runtime_error& error)
    {
      // Error occured while trying to gather image.  Retry, don't exit.
      std::stringstream ss;
      ss << "VideoBuffer exception: " << error.what();
      dispatcher->log_error( ss.str() );
    }
    catch (cv::Exception e)
    {
      // OpenCV Exception occured while trying to gather image.  Retry, don't exit.
      std::stringstream ss;
      ss << "VideoBuffer OpenCV exception: " << e.what();
      dispatcher->log_error( ss.str() );
    }
    
    // Delay 1 second
    //Sleep(1000);
    QThread::msleep(1000);
    
  }

  
}


// Continuously grabs images from the video capture.  If there is an error,
// it returns so that the video capture can be recreated.
void getImages(cv::VideoCapture cap, VideoDispatcher* dispatcher)
{

  while (dispatcher->active)
  {
    while (dispatcher->active)
    {
      
      bool hasImage = false;
      try
      {
        cv::Mat frame;
	hasImage = cap.read(frame);
		  // Double check the image to make sure it's valid.
	if (!frame.data || frame.empty())
	{
	  std::stringstream ss;
	  ss << "Stream " << dispatcher->mjpeg_url << " received invalid frame";
	  dispatcher->log_error(ss.str());
	  return;
	}
	
	dispatcher->mMutex.lock();
	dispatcher->setLatestFrame(frame);
	dispatcher->mMutex.unlock();
      }
      catch (const std::runtime_error& error)
      {
	// Error occured while trying to gather image.  Retry, don't exit.
	std::stringstream ss;
	ss << "Exception happened " <<  error.what();
	dispatcher->log_error(ss.str());
	dispatcher->mMutex.unlock();
	return;
      }

      
      dispatcher->mMutex.unlock();
      
      if (hasImage == false)
	break;
      
      // Delay 15ms
      //Sleep(15);
      QThread::msleep(15);
    }
    
    // Delay 100ms
    //Sleep(100);
    QThread::msleep(100);
  }

  cout << "thread ended \n";

}
