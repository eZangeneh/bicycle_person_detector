QT += core
QT -= gui

CONFIG += c++11

TARGET = pedesterian_detection
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

#CONFIG += link_pkgconfig
#PKGCONFIG += opencv
INCLUDEPATH += "$$PWD/opencv/include"
LIBS += -L"$$PWD/opencv/vc12/lib" \
-lopencv_core249 \
-lopencv_highgui249 \
-lopencv_imgproc249 \
-lopencv_video249 \
-lopencv_legacy249 \
-lopencv_contrib249 \
-lopencv_features2d249 \


SOURCES += main.cpp \
    videobuffer.cpp \
    tinythread.cpp \
    findcontour_boundingrect.cpp \
    Detector/Private/ACF/Mex/convConst.cpp \
    Detector/Private/ACF/Mex/gradientMex.cpp \
    Detector/Private/ACF/Mex/wrappers.cpp \
    Detector/Private/ACF/clsACFPrivate.cpp \
    Detector/Private/ACF/clsChannelFeatures.cpp \
    Detector/Private/ACF/clsPyramid.cpp \
    Detector/clsACF.cpp \
    clspedestriandetector.cpp

HEADERS += \
    videobuffer.h \
    tinythread.h \
    Detector/Private/ACF/Mex/convConst.hpp \
    Detector/Private/ACF/Mex/gradientMex.hpp \
    Detector/Private/ACF/Mex/imPadMex.hpp \
    Detector/Private/ACF/Mex/imResampleMex.hpp \
    Detector/Private/ACF/Mex/rgbConvertMex.hpp \
    Detector/Private/ACF/Mex/sse.hpp \
    Detector/Private/ACF/Mex/wrappers.hpp \
    Detector/Private/ACF/clsACFPrivate.h \
    Detector/Private/ACF/clsChannelFeatures.h \
    Detector/Private/ACF/clsPyramid.h \
    Detector/Private/ACF/types.h \
    Detector/clsACF.h \
    Detector/intfDetector.hpp \
    clspedestriandetector.h

#INCLUDEPATH += /home/tuey/development/faraadid/FaraadidLibraries/trunk/out/include/
#LIBS += -L/home/tuey/development/faraadid/FaraadidLibraries/trunk/out/lib -lFD_VisionCore_ObjectDetection -lFD_Common
#LIBS += -lTargomanCommon
#LIBS += /home/tuey/development/faraadid/FaraadidLibraries/trunk/out/lib/libFD_VisionCore_ObjectDetection.so.0
