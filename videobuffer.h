#ifndef OPENALPR_VIDEOBUFFER_H
#define OPENALPR_VIDEOBUFFER_H

#include <cstdio>
#include <stdexcept>
#include <sstream>
#include <iostream>
#include "opencv2/highgui/highgui.hpp"

//#include "support/filesystem.h"
#include "tinythread.h"
//#include "support/platform.h"



class VideoDispatcher
{
  public:
    VideoDispatcher(std::string mjpeg_url, int fps)
    {
      this->active = true;
      this->latestFrameNumber = -1;
      this->lastFrameRead = -1;
      this->fps = fps;
      this->mjpeg_url = mjpeg_url;
    }
    
    
    int getLatestFrame(cv::Mat* frame)
    {
    

      if (latestFrameNumber == lastFrameRead)
        return -1;
      frame->create(latestFrame.size(), latestFrame.type());
      latestFrame.copyTo(*frame);
      
      this->lastFrameRead = this->latestFrameNumber;           
      
      return this->lastFrameRead;
    }
    
    void setLatestFrame(cv::Mat frame)
    {      
      frame.copyTo(this->latestFrame);

      
      this->latestFrameNumber++;
	  if (this->latestFrameNumber >= 5000)
		  this->latestFrameNumber = 0;
    }
    
    virtual void log_info(std::string message)
    {
      std::cout << message << std::endl;
    }
    virtual void log_error(std::string error)
    {
      std::cerr << error << std::endl;
    }
    
    bool active;
    
    int latestFrameNumber;
    int lastFrameRead;
    
    std::string mjpeg_url;
    int fps;
    tthread::mutex mMutex;
    
  private:
    cv::Mat latestFrame;
};

class VideoBuffer
{

  public:
    VideoBuffer();
    virtual ~VideoBuffer();

    void connect(std::string mjpeg_url, int fps);
    

    // If a new frame is available, the function sets "frame" to it and returns the frame number
    // If no frames are available, or the latest has already been grabbed, returns -1.
    // regionsOfInterest is set to a list of good regions to check for license plates.  Default is one rectangle for the entire frame.
    int getLatestFrame(cv::Mat* frame);

    void disconnect();
    
  protected:
  
    virtual VideoDispatcher* createDispatcher(std::string mjpeg_url, int fps);
    
  private:
    
    
    VideoDispatcher* dispatcher;
};




#endif // OPENALPR_VIDEOBUFFER_H
