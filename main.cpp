#include <QCoreApplication>
#include <QTime>
#include <QList>
#include <QVector>
#include <QMap>
#include <QDebug>

#include<iostream>
#include <algorithm>
#include <limits>
#include <clspedestriandetector.h>
#include <videobuffer.h>
#include <tinythread.h>
#include <ctime>
#include <stdio.h>
#include <algorithm>
#include<opencv2/opencv.hpp>
#include<opencv/cv.h>

//#include "dirent.h"
#include "Detector/clsACF.h"
#include "Detector/intfDetector.hpp"
using namespace cv;
using namespace FD::VisionCore;
using namespace FD::VisionCore::ObjectDetection;
using namespace FD;
int main()
{

//    cv::Mat frame;
    cv::CascadeClassifier cascadeHS;
    if(!cascadeHS.load("models/HS.xml"))
        qDebug()<<"Error in Loading";

    cv::CascadeClassifier cascadeFB;
    if(!cascadeFB.load("models/haarcascade_fullbody.xml"))
        qDebug()<<"Error in Loading";

    cv::CascadeClassifier cascadeLB;
    if(!cascadeLB.load("models/haarcascade_lowerbody.xml"))
        qDebug()<<"Error in Loading";

    cv::CascadeClassifier cascadeUB;
    if(!cascadeUB.load("models/haarcascade_upperbody.xml"))
        qDebug()<<"Error in Loading";

    clsPedestrianDetector obj_clsDetector;
    FD::VisionCore::ObjectDetection::clsACF detACF;
    detACF.init();

    //adreese aksha
    QString Testpath = "bikes_and_persons/bikes_and_persons";
    QDir dir(Testpath);
    QStringList imageName = dir.entryList(QDir::Files);

    for(int i=0; i<imageName.size(); i++)
    {
        std::vector<cv::Rect> detectedHS;
        std::vector<cv::Rect> detectedFB;
        std::vector<cv::Rect> detectedUB;
        std::vector<cv::Rect> detectedLB;
        std::vector<cv::Rect> human_rects;
        std::vector<cv::Rect> face_rects;
        std::vector<cv::Rect> constructed_human_rects;

        QString filename = Testpath + QString("/") + imageName[i];

        //khandane aks ha
        cv::Mat frame = cv::imread(filename.toStdString());




        cv::Rect detectedhuman;

        //an rect haee ke motmaeen mishavim pedestrian ast ra dar in vector gharar midahim
        QVector<cv::Rect> pedestrianDetected;

        //HOG detection
        obj_clsDetector.HumanDetector(frame, human_rects, constructed_human_rects,obj_clsDetector.hard);

        //Haar detection
        cascadeFB.detectMultiScale(frame,detectedFB,1.1,2,0,cv::Size(20,50));
        cascadeHS.detectMultiScale(frame,detectedHS,1.1,2,0,cv::Size(20,20));

        //ACF detection
        QList<stuDetectedObject> detectedPedAcf = detACF.detectObjects(frame);
        cv::Rect r(0,0,frame.cols,frame.rows) ;

        //detect face with Haar
        obj_clsDetector.face_Detector(frame, r,face_rects);


        // vector to QVector
        QVector<cv::Rect> detHaarHS;detHaarHS = QVector<cv::Rect>::fromStdVector(detectedHS);
        QVector<cv::Rect> detHaarFB;detHaarFB = QVector<cv::Rect>::fromStdVector(detectedFB);
        QVector<cv::Rect> detHaarFace;detHaarFace = QVector<cv::Rect>::fromStdVector(face_rects);
        QVector<cv::Rect> detHOGPed;detHOGPed = QVector<cv::Rect>::fromStdVector(human_rects);
        QVector<stuDetectedObject> detACFPed;detACFPed = detectedPedAcf.toVector();


/// Overlap between full bodies
///
        //check for overlap between full body in ACF, Haar
        for(int fbCounter = 0; fbCounter < detHaarFB.size(); fbCounter++)
        {
            for(int i=0; i<detACFPed.size(); i++)
            {
                double intersect = (detACFPed[i] & detHaarFB[fbCounter]).area();
                double minArea = detACFPed[i].area() < detHaarFB[fbCounter].area() ? detACFPed[i].area() : detHaarFB[fbCounter].area() ;
                if( (intersect / minArea) > 0.80)
                {
                    pedestrianDetected.push_back(detACFPed[i]);
                    detACFPed.removeAt(i);
                    detHaarFB.removeAt(fbCounter);
                    break;
                }
            }
        }

        //check for overlap between full body in ACF, HOG
        for(int fbCounter = 0; fbCounter < detHOGPed.size(); fbCounter++)
        {
            for(int i=0; i<detACFPed.size(); i++)
            {
                double intersect = (detACFPed[i] & detHOGPed[fbCounter]).area();
                double minArea = detACFPed[i].area() < detHOGPed[fbCounter].area() ? detACFPed[i].area() : detHOGPed[fbCounter].area() ;
                if( (intersect / minArea) > 0.80)
                {
                    pedestrianDetected.push_back(detACFPed[i]);
                    detACFPed.removeAt(i);
                    detHOGPed.removeAt(fbCounter);
                    break;
                }
            }
        }

        //check for overlap between full body in Haar, HOG
        for(int fbCounter = 0; fbCounter < detHOGPed.size(); fbCounter++)
        {
            for(int i=0; i<detHaarFB.size(); i++)
            {
                double intersect = (detHaarFB[i] & detHOGPed[fbCounter]).area();
                double minArea = detHaarFB[i].area() < detHOGPed[fbCounter].area() ? detHaarFB[i].area() : detHOGPed[fbCounter].area() ;
                if( (intersect / minArea) > 0.80)
                {
                    pedestrianDetected.push_back(detHaarFB[i]);
                    detHaarFB.removeAt(i);
                    detHOGPed.removeAt(fbCounter);
                    break;
                }
            }
        }



/// Check if there is head and shoulder in body
        QVector<cv::Rect> remainBody;
        remainBody += detHaarFace;
        remainBody += detHOGPed;
        for(int i=0; i<detACFPed.size(); i++)
            remainBody.push_back(detACFPed[i]);


        // aya head and shoulder darone har kodam az in ha hast ya na
        for(int i=0; i<remainBody.size(); i++)
        {
            for(int j=0; j<detHaarHS.size(); j++)
            {
                double intersect = (remainBody[i] & detHaarHS[j]).area();
                double minArea = remainBody[i].area() < detHaarHS[j].area() ? remainBody[i].area() : detHaarHS[j].area() ;
                qDebug()<< "InterSect : "<<(intersect / minArea);
                if( (intersect / minArea) > 0.50)
                {
                    pedestrianDetected.push_back(remainBody[i]);
                    remainBody.removeAt(i);
                    i--;
                    break;
                }
            }
        }


/// check aspectRatio
    for(int i=0; i<pedestrianDetected.size(); i++)
    {
        qDebug()<<"Ratio : "<<(double)pedestrianDetected[i].height / pedestrianDetected[i].width;
        if((double)pedestrianDetected[i].height / pedestrianDetected[i].width < 2)
        {
            pedestrianDetected.remove(i);
            i--;
        }
    }



///delete Full Overlap
        for(int i=0;i<pedestrianDetected.size();i++)
        {
            for(int j=0; j<pedestrianDetected.size(); j++)
            {
                if(i != j)
                {
                    double intersect = (pedestrianDetected[i] & pedestrianDetected[j]).area();
                    double minArea = pedestrianDetected[i].area() < pedestrianDetected[j].area() ? pedestrianDetected[i].area() : pedestrianDetected[j].area() ;
                    qDebug()<< "InterSect : "<<(intersect / minArea);
                    if( (intersect / minArea) > 0.50)
                    {
                        pedestrianDetected.removeAt(j);
                        j--;
                    }
                }
            }
        }


        // Our Method
        for(int i=0; i<pedestrianDetected.size(); i++)
            cv::rectangle(frame, pedestrianDetected[i], cv::Scalar(255,255,255),2);

        //HOG
        for(int i=0; i<human_rects.size(); i++)
            cv::rectangle(frame, human_rects[i], cv::Scalar(255,0,0));

        //ACF
        for(int i=0; i<detectedPedAcf.size(); i++)
            cv::rectangle(frame, detectedPedAcf[i], cv::Scalar(0,0,255));

        //HAAR
        for(int i=0; i<detectedFB.size(); i++)
            cv::rectangle(frame, detectedFB[i], cv::Scalar(0,0,255));




        cv::imshow("ResultOfBoth",frame);
        cv::waitKey(0);
    }




}
